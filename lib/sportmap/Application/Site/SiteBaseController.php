<?php

  namespace Lib\Application\Site;

  use Symfony\Bundle\FrameworkBundle\Controller\Controller;

  /**
   * @package Lib\Application
   */
  class SiteBaseController extends Controller {

  }