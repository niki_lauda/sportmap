<?php

  namespace Lib\Application;

  use Lib\Html\LinkItem;

  /**
   *
   * @package Lib\Application
   */
  abstract class BaseController extends \Symfony\Bundle\FrameworkBundle\Controller\Controller {

    /**
     * @return \SiteBundle\Entity\Users\User
     */
    protected function getUser() {
      return parent::getUser();
    }


    /**
     * @param LinkItem $linkItem
     */
    protected function addBreadCrumb(LinkItem $linkItem) {
      $this->get("white_october_breadcrumbs")
        ->addItem($linkItem->getTitle(), $linkItem->getUrl());
    }

  }