<?php
  namespace Lib\Application;

  /**
   *
   * @package Lib\Core\Environment
   */
  class Environment {

    /**
     * @var null
     */
    protected static $devBrunch = null;

    /**
     * @var bool
     */
    protected static $isDebug = false;


    /**
     * @return null
     */
    public static function getDevBrunch() {
      return static::$devBrunch;
    }


    /**
     * @param $devBrunch
     */
    public static function setDevBrunch($devBrunch) {
      self::$devBrunch = $devBrunch;
    }


    /**
     * @param $isDebug
     */
    public static function setIsDebug($isDebug) {
      self::$isDebug = (bool) $isDebug;
    }


    /**
     * @return bool
     */
    public static function isDebug() {
      return self::$isDebug;
    }

  }