<?php

  namespace Lib\Application\Db;

  use Doctrine\ORM\QueryBuilder;
  use Symfony\Component\HttpFoundation\Request;


  /**
   * @package Lib\Db
   */
  class Paginator {

    /**
     * @var int
     */
    protected $currentPage = 1;

    /**
     * @var int
     */
    protected $lastPage = 1;

    /**
     * @var int
     */
    protected $itemsNum = 0;

    /**
     * @var int
     */
    protected $perPage = 0;


    /**
     * @param int $itemsNum
     * @param int $currentPage
     * @param int $perPage
     */
    public function __construct($itemsNum, $currentPage, $perPage) {
      $this->itemsNum = $itemsNum;
      $this->currentPage = $currentPage;
      $this->perPage = $perPage;
      if ($perPage > 0 and $itemsNum > 0) {
        $this->lastPage = ceil($itemsNum / $perPage);
      }
    }


    /**
     * @param Request $request
     * @param int $itemsNum
     * @return static
     */
    public static function createFromRequest(Request $request, $itemsNum) {
      return new static(
        $itemsNum,
        $request->query->getInt('page', 1),
        $request->query->getInt('perPage', 15)
      );
    }


    /**
     * @return int
     */
    public function getCurrentPage() {
      return $this->currentPage;
    }


    /**
     * @return int
     */
    public function getLastPage() {
      return $this->lastPage;
    }


    /**
     * @return int
     */
    public function getItemsNum() {
      return $this->itemsNum;
    }


    /**
     * @return int
     */
    public function getPerPage() {
      return $this->perPage;
    }


    /**
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function prepareQuery(QueryBuilder $query) {
      if ($this->getCurrentPage() > 1) {
        $query->setFirstResult(($this->getCurrentPage() - 1) * $this->getPerPage());
      }
      $query->setMaxResults($this->getPerPage());
      return $query;
    }


    /**
     * @return int
     */
    public function getOffsetNum() {
      return $this->perPage * ($this->currentPage - 1);
    }

  }