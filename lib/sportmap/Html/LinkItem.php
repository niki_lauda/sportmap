<?php

  namespace Lib\Html;

  /**
   * Class LinkItem
   * @package Lib\Html
   */
  class LinkItem {

    /**
     * @var
     */
    protected $title;

    /**
     * @var
     */
    protected $url;


    /**
     * @param string $title
     * @param string $link
     */
    public function __construct($title = '', $link = null) {
      $this->title = (string) $title;
      $this->link = (string) $link;
    }


    /**
     * @return mixed
     */
    public function getTitle() {
      return $this->title;
    }


    /**
     * @param mixed $title
     */
    public function setTitle($title) {
      $this->title = $title;
    }


    /**
     * @return mixed
     */
    public function getUrl() {
      return $this->url;
    }


    /**
     * @param mixed $url
     */
    public function setUrl($url) {
      $this->url = $url;
    }


  }