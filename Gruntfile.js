module.exports = function (grunt) {

  var jsFrontendFiles = [
    './bower_components/jquery/dist/jquery.js'
  ];

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    copy: {
      jquery: {
        expand: true,
        cwd: 'bower_components/jquery/',
        src: ['**'],
        dest: 'static/libs/jquery/'
      },
      jquery_ui: {
        expand: true,
        cwd: 'bower_components/jquery-ui/',
        src: ['**'],
        dest: 'static/libs/jquery_ui/'
      },
      bootstrap: {
        expand: true,
        cwd: 'bower_components/bootstrap/',
        src: ['**'],
        dest: 'static/libs/bootstrap/'
      }
    },
    uglify: {
      jsFrontend: {
        options: {
          mangle: false,
          sourceMap: false
        },
        files: {
          './static/js/frontend.min.js': jsFrontendFiles
        }
      }
    },
    sass: {
      dist: {
        options: {
          sourcemap: 'none',
          style: 'compressed'
        },
        files: [
          {
            expand: true,
            cwd: './static/css/builds/',
            src: ['*.scss'],
            ext: '.css',
            dest: './static/css/compiled/'
          },
          {
            expand: true,
            cwd: './static/css/builds/parts/',
            src: ['*.scss'],
            ext: '.css',
            dest: './static/css/compiled/parts/'
          }
        ]
      }
    },
    watch: {
      css: {
        files: [
          "./static/css/builds/*",
          "./static/css/builds/global/*",
          "./static/css/builds/parts/*"
        ],
        tasks: ["sass"]
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('default', []);
  grunt.registerTask('all', ['exec:npm', 'exec:bower', 'copy', 'concat', 'uglify', 'sass']);

};
