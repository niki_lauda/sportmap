<?php

  namespace SiteBundle\Entity\Cats;

  use Doctrine\ORM\Mapping as ORM;

  /**
   * CatsModel
   *
   * @ORM\Table(name="cats")
   * @ORM\Entity(repositoryClass="SiteBundle\Repository\Cats\CatsModelRepository")
   */
  class CatsModel {

    CONST TYPE_EXERCISES = 'E';

    CONST TYPE_GAMES = 'G';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=1, options={"default":""})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $type = self::TYPE_EXERCISES;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", options={"default":""})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
      return $this->id;
    }


    /**
     * @return string
     */
    public function getName() {
      return $this->name;
    }


    /**
     * @param string $name
     */
    public function setName($name) {
      $this->name = $name;
    }


    /**
     * @return string
     */
    public function getType() {
      return $this->type;
    }


    /**
     * @param string $type
     */
    public function setType($type) {
      $this->type = $type;
    }

  }

