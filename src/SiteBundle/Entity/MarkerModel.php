<?php

  namespace SiteBundle\Entity;

  use Doctrine\ORM\Mapping as ORM;
  use SiteBundle\Entity\Cats\CatsModel;
  use SiteBundle\Entity\Users\User;

  /**
   * MarkerModel
   *
   * @ORM\Table(name="markers")
   * @ORM\Entity(repositoryClass="SiteBundle\Repository\MarkerModelRepository")
   */
  class MarkerModel {

    const STATUS_ACTIVE = 'A';

    const STATUS_MODERATION = 'M';

    const STATUS_TRASH = 'T';


    /**
     * @var int
     *
     * @ORM\Column(type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var CatsModel[]
     *
     * @ORM\ManyToMany(targetEntity="\SiteBundle\Entity\Cats\CatsModel")
     * @ORM\JoinTable(name="cats_markers")
     */
    private $categories;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="SiteBundle\Entity\Users\User")
     */
    private $user;


    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1, options={"default":"M"})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $status = self::STATUS_MODERATION;

    /**
     * @var float
     *
     * @ORM\Column(type="float", options={"default":0})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $position_lat = 0;


    /**
     * @var float
     *
     * @ORM\Column(type="float", options={"default":0})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $position_lng = 0;

    /**
     * @var string
     *
     * @ORM\Column(type="string", options={"default":""})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $title = '';

    /**
     * @var string
     *
     * @ORM\Column(type="string", options={"default":""})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $address = '';


    /**
     * @var string
     *
     * @ORM\Column(type="string", options={"default":""})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $description = '';

    /**
     * @var string
     *
     * @ORM\Column(type="text", options={"default":""})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $imageUrls = '';


    /**
     * @return int
     */
    public function getId() {
      return $this->id;
    }


    /**
     * @return CatsModel[]
     */
    public function getCategories() {
      return $this->categories;
    }


    /**
     * @param CatsModel $category
     * @return static
     */
    public function addCategory(CatsModel $category) {
      $this->categories[] = $category;
      return $this;
    }


    /**
     * @param CatsModel $category
     * @return static
     */
    public function removeCategory(CatsModel $category) {
      foreach ($this->categories as $key => $item) {
        /** @var CatsModel $item */
        if ($item->getId() == $category->getId()) {
          unset($this->categories[$key]);
        }
      }
      return $this;
    }


    /**
     * @param CatsModel $category
     * @return bool
     */
    public function hasCategory(CatsModel $category) {
      foreach ($this->categories as $item) {
        /** @var CatsModel $item */
        if ($item->getId() == $category->getId()) {
          return true;
        }
      }
      return false;
    }


    /**
     * @param CatsModel[] $categories
     * @return static
     */
    public function setCategories($categories) {
      $this->categories = $categories;
      return $this;
    }


    /**
     * @return User
     */
    public function getUser() {
      return $this->user;
    }


    /**
     * @param mixed $user
     */
    public function setUser($user) {
      $this->user = $user;
    }


    /**
     * @return string
     */
    public function getTitle() {
      return $this->title;
    }


    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title) {
      $this->title = $title;
      return $this;
    }


    /**
     * @return float
     */
    public function getPositionLat() {
      return $this->position_lat;
    }


    /**
     * @param float $position_lat
     * @return $this
     */
    public function setPositionLat($position_lat) {
      $this->position_lat = $position_lat;
      return $this;
    }


    /**
     * @return float
     */
    public function getPositionLng() {
      return $this->position_lng;
    }


    /**
     * @param float $position_lng
     * @return $this
     */
    public function setPositionLng($position_lng) {
      $this->position_lng = $position_lng;
      return $this;
    }


    /**
     * @return string
     */
    public function getAddress() {
      return $this->address;
    }


    /**
     * @param string $address
     */
    public function setAddress($address) {
      $this->address = $address;
    }


    /**
     * @return string
     */
    public function getDescription() {
      return $this->description;
    }


    /**
     * @return string
     */
    public function getShortDescription() {
      $short = explode(".", $this->getDescription());
      if (count($short) > 0) {
        return $short[0] . '.';
      }
      return $this->getDescription();
    }


    /**
     * @param string $description
     */
    public function setDescription($description) {
      $this->description = $description;
    }


    /**
     * @return array
     */
    public function getImgUrls() {
      return (array) json_decode($this->imageUrls, true);
    }


    /**
     * @param string $imageUrl
     * @return $this
     */
    public function addImgUrl($imageUrl) {
      $existUrls = $this->getImgUrls();
      $existUrls[] = $imageUrl;
      $this->setImgUrlList($existUrls);
      return $this;
    }


    /**
     * @param array $imageUrlList
     * @return $this
     */
    public function setImgUrlList(array $imageUrlList) {
      $this->imageUrls = json_encode($imageUrlList);
      return $this;
    }


    /**
     * @return string
     */
    public function getMainImageUrl() {
      $images = $this->getImgUrls();
      if (empty($images[0])) {
        return 'http://research.larc.smu.edu.sg/ge2015/assets/img/default_photo.png';
      }
      return $images[0];
    }


    /**
     * @param string $removeUrl
     * @return $this
     */
    public function removeImageUrl($removeUrl) {
      $imageUrlList = $this->getImgUrls();
      foreach ($imageUrlList as $key => $url) {
        if ($url == $removeUrl) {
          unset($imageUrlList[$key]);
        }
      }
      $this->setImgUrlList($imageUrlList);
      return $this;
    }


    /**
     * @param string $status
     * @return string
     */
    public function isStatus($status) {
      return $this->status == $status;
    }


    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status) {
      $this->status = $status;
      return $this;
    }


    /**
     * @return array
     */
    public function getStatusNames() {
      return [
        self::STATUS_ACTIVE => 'Активний',
        self::STATUS_MODERATION => 'На модерації',
        self::STATUS_TRASH => 'Корзина',
      ];
    }
  }
