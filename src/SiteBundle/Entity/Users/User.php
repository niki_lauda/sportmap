<?php

  namespace SiteBundle\Entity\Users;

  use Doctrine\ORM\Mapping as ORM;
  use Symfony\Component\Security\Core\User\UserInterface;

  /**
   * User
   *
   * @ORM\Table(name="users")
   * @ORM\Entity(repositoryClass="SiteBundle\Repository\Users\UserRepository")
   */
  class User implements UserInterface {

    CONST ROLE_USER = 'ROLE_USER';

    CONST ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=30, options={"default":""})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $username = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="role_list", type="text")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $roleList;


    /**
     * @return int
     */
    public function getId() {
      return $this->id;
    }


    /**
     * @param int $id
     * @return $this
     */
    public function setId($id) {
      $this->id = $id;
      return $this;
    }


    /**
     * @return string
     */
    public function getUsername() {
      return $this->username;
    }


    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username) {
      $this->username = $username;
      return $this;
    }


    /**
     * @return string
     */
    public function getEmail() {
      return $this->email;
    }


    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email) {
      $this->email = $email;
      return $this;
    }


    /**
     * @return string
     */
    public function getPassword() {
      return $this->password;
    }


    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password) {
      $this->password = $password;
      return $this;
    }


    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return string[] The user roles
     */
    public function getRoles() {
      return explode(',', $this->roleList);
    }


    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles){
      $this->roleList = implode(';', $roles);
      return $this;
    }


    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt() {
      return null;
    }


    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials() {

    }
  }

