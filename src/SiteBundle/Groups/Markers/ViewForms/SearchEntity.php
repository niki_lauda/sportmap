<?php

/**
 * Created by PhpStorm.
 * User: yevhenii
 * Date: 30.04.16
 * Time: 20:01
 */
namespace SiteBundle\Groups\Markers\ViewForms;
use Symfony\Component\Validator\Constraints as Assert;

class SearchEntity
{
    protected $sportCategory;
    protected $city;

    /**
     * @return mixed
     */
    public function getSportCategory()
    {
        return $this->sportCategory;
    }

    /**
     * @param mixed $sportCategory
     */
    public function setSportCategory($sportCategory)
    {
        $this->sportCategory = $sportCategory;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }
}