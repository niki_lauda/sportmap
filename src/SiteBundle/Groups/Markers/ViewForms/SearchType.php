<?php
/**
 * Created by PhpStorm.
 * User: yevhenii
 * Date: 30.04.16
 * Time: 19:25
 */
namespace SiteBundle\Groups\Markers\ViewForms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sportCategory', ChoiceType::class, array(
                'choices'  => array(
                    'Все' => 'все',
                    'Футбол' => 'футбол',
                    'Баскетбол' => 'баскетбол',
                    'Волейбол' => 'волейбол',
                )
            ))
            ->add('city', TextType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SiteBundle\Groups\Markers\ViewForms\SearchEntity'
        ));
    }
}