<?php

  namespace SiteBundle\Groups\Markers\Controller;

  use Lib\Application\BaseController;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use Symfony\Component\HttpFoundation\Request;

  /**
   *
   * @package SiteBundle\User\Controller
   */
  class SiteIndexController extends BaseController {

    /**
     * @Route("/", name="site-home")
     */
    public function indexAction(Request $request) {
      return $this->render('markers/index.html.twig');
    }
  }
