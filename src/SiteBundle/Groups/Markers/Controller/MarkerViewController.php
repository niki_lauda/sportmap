<?php

namespace SiteBundle\Groups\Markers\Controller;

use Lib\Application\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use SiteBundle\Groups\Markers\ViewForms\SearchEntity;
use SiteBundle\Groups\Markers\ViewForms\SearchType;
use Lib\Html\LinkItem;

/**
 *
 * @package SiteBundle\User\Controller
 */
class MarkerViewController extends BaseController
{
    /**
     * @Route("/markers/{markerID}", requirements={"markerID": "\d+"} , name="markerView")
     */
    public function indexAction($markerID){
        $this->addBreadCrumb(new LinkItem('Перегляд обраного місця'));
        $marker = $this->getDoctrine()->getRepository('SiteBundle:MarkerModel')->findOneById($markerID);

        return $this->render('markers/marker.html.twig', array(
            'marker' => $marker,
        ));
    }
}
