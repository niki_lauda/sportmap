<?php

  namespace SiteBundle\Groups\Markers\Controller;

  use Lib\Application\BaseController;
  use Lib\Html\LinkItem;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use SiteBundle\Entity\Cats\CatsModel;
  use SiteBundle\Entity\MarkerModel;
  use Symfony\Component\HttpFoundation\Request;
  use SiteBundle\Groups\Markers\ViewForms\SearchEntity;
  use SiteBundle\Groups\Markers\ViewForms\SearchType;

  /**
   *
   * @package SiteBundle\User\Controller
   */
  class MarkerCreateController extends BaseController {

    /**
     * @Route("/markers/create/", name="site-marker-create")
     */
    public function indexAction(Request $request) {
      if (!$this->getUser()) {
        return $this->redirect($this->generateUrl('site-user-login'));
      }

      $this->addBreadCrumb(new LinkItem('Додавання нового місця на карту'));

      $catsRepository = $this->getDoctrine()->getRepository(CatsModel::class);
      /** @var CatsModel[] $cats */
      $cats = $catsRepository->findAll();

      if ($request->isMethod(Request::METHOD_POST)) {

        $marker = new MarkerModel();
        $marker->setTitle($request->request->get('title', ''));
        $marker->setStatus(MarkerModel::STATUS_MODERATION);
        $marker->setAddress($request->request->get('address', ''));
        $marker->setDescription($request->request->get('description', ''));
        $marker->setPositionLat($request->request->get('position_lat'));
        $marker->setPositionLng($request->request->get('position_lng'));
        $marker->setUser($this->getUser());

        //@todo validation

        $this->getDoctrine()->getManager()->persist($marker);
        $this->getDoctrine()->getManager()->flush();
      }

      return $this->render('markers/create.html.twig', [
        'cats' => $cats,
      ]);
    }

  }
