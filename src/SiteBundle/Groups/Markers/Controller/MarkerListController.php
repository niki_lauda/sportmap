<?php

namespace SiteBundle\Groups\Markers\Controller;

use Lib\Application\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use SiteBundle\Groups\Markers\ViewForms\SearchEntity;
use SiteBundle\Groups\Markers\ViewForms\SearchType;
use Lib\Html\LinkItem;

/**
 *
 * @package SiteBundle\User\Controller
 */
class MarkerListController extends BaseController
{

    private $PAGINATOR_COUNT = 10;

    /**
     * @Route("/markers/list/{pageNumber}", defaults={"pageNumber" = 0}, requirements={"pageNumber": "\d+"} , name="markerList")
     */
    public function indexAction($pageNumber){
        $this->addBreadCrumb(new LinkItem('Список всіх спортивних місць'));
        $markers = $this->getDoctrine()->getRepository('SiteBundle:MarkerModel')->findBy(
            array(),array(), $this->PAGINATOR_COUNT, $pageNumber * $this->PAGINATOR_COUNT);

        $queryBuilder = $this->getDoctrine()->getManager()->getRepository('SiteBundle:MarkerModel')->createQueryBuilder('a');
        $queryBuilder->select('COUNT(a)');
        $markersCount = $queryBuilder->getQuery()->getSingleScalarResult();

        return $this->render('markers/list.html.twig', array(
            'markers' => $markers,
            'pageNumber' => $pageNumber,
            'maxPageNumber' => $this->maxPageNumber($markersCount),
            'count' => $markersCount
        ));
    }

    private function maxPageNumber($markersCount){
        if($markersCount % $this->PAGINATOR_COUNT == 0)
            return $markersCount / $this->PAGINATOR_COUNT;
        return floor($markersCount / $this->PAGINATOR_COUNT);
    }

    private function sliceMarkers($markers, $pageNumber){
        $markersCount = count($markers);
        if ($markersCount > $this->PAGINATOR_COUNT) {
            if ($markersCount % $this->PAGINATOR_COUNT == 0)
                $markers = array_slice($markers, $pageNumber * $this->PAGINATOR_COUNT, $this->PAGINATOR_COUNT);
            else
                $markers = array_slice($markers, $pageNumber * $this->PAGINATOR_COUNT, $markersCount % $this->PAGINATOR_COUNT);
        }
        return $markers;
    }
}
