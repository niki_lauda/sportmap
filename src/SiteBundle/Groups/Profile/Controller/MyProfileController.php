<?php

  namespace SiteBundle\Groups\Profile\Controller;

  use Lib\Application\BaseController;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use Symfony\Component\HttpFoundation\Request;
  use Symfony\Component\HttpFoundation\Response;

  /**
   *
   * @package SiteBundle\Groups\User\Controller
   */
  class MyProfileController extends BaseController {

    /**
     * @Route("/my/profile/", name="myProfile")
     */
    public function indexAction(Request $request) {
      return new Response('profile');
    }
  }