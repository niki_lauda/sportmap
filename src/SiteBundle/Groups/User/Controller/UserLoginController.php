<?php

  namespace SiteBundle\Groups\User\Controller;

  use Lib\Html\LinkItem;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use Symfony\Component\HttpFoundation\Request;
  use Symfony\Component\HttpFoundation\Response;

  /**
   *
   * @package SiteBundle\Groups\User\Controller
   */
  class UserLoginController extends \Lib\Application\BaseController {

    /**
     * @Route("/user/login/", name="site-user-login")
     */
    public function indexAction(Request $request) {
      $this->addBreadCrumb(new LinkItem('Авторизація'));

      $authenticationUtils = $this->get('security.authentication_utils');

      // get the login error if there is one
      $error = $authenticationUtils->getLastAuthenticationError();
      if(!empty($error)){
        $error = 'Неправильний логін і/або пароль';
      }

      // last username entered by the user
      $lastUsername = $authenticationUtils->getLastUsername();

      return $this->render(
        'user/login.html.twig',
        array(
          // last username entered by the user
          'last_username' => $lastUsername,
          'error' => $error,
        )
      );
    }

  }