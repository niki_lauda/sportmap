<?php

  namespace SiteBundle\Groups\User\Controller;

  use Lib\Html\LinkItem;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use SiteBundle\Entity\Users\User;
  use Symfony\Component\HttpFoundation\Request;
  use Symfony\Component\HttpFoundation\Response;

  /**
   *
   * @package SiteBundle\Groups\User\Controller
   */
  class UserRegisterController extends \Lib\Application\BaseController {

    /**
     * @Route("/user/register/", name="register")
     */
    public function indexAction(Request $request) {
      $this->addBreadCrumb(new LinkItem('Реєстрація'));

      $errors = [];
      if ($request->isMethod(Request::METHOD_POST)) {
        $email = $request->request->get('email');
        $password = $request->request->get('password');
        $errors = $this->validateRequest($email, $password);
        if (empty($errors)) {
          $newUser = new User();
          $newUser->setEmail($email);
          $newUser->setPassword($password);
          $newUser->setRoles([User::ROLE_USER]);

          $this->getDoctrine()->getManager()->persist($newUser);
          $this->getDoctrine()->getManager()->flush();

          return $this->redirect($this->generateUrl('register-success'));
        }
      }

      return $this->render('user/register.html.twig', [
        'errors' => $errors
      ]);
    }


    /**
     * @param string $email
     * @param string $password
     * @return \string[]
     */
    private function validateRequest($email, $password) {
      $errors = [];
      if (empty($password)) {
        $errors[] = 'Придумайте пароль';
      }
      if (preg_match('!^[a-z0-9\_\-\.]+\@[a-z0-9\_\-\.]+\.[a-z]+$!', $email)) {
        $repository = $this->getDoctrine()->getRepository(User::class);
        $query = $repository->createQueryBuilder('user');
        $query->where('user.email = :email');
        $query->setParameter('email', $email);
        $query->select('count(user.id)');

        if ($query->getQuery()->getSingleScalarResult() >= 1) {
          $errors[] = 'Користувач з даним email вже зареєстрований';
        }
      } else {
        $errors[] = 'Неправильний email!';
      }
      return $errors;
    }


    /**
     * @Route("/user/register/success/", name="register-success")
     */
    public function successAction(Request $request) {
      return $this->render('user/registerSuccess.html.twig');
    }

  }