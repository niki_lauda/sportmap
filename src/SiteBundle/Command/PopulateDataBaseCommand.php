<?php
  namespace SiteBundle\Command;

  use SiteBundle\Entity\MarkerModel;
  use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
  use Symfony\Component\Console\Input\InputInterface;
  use Symfony\Component\Console\Output\OutputInterface;

  /**
   * Class PopulateDataBaseCommand
   * @package SiteBundle\Command
   */
  class PopulateDataBaseCommand extends ContainerAwareCommand {

    /**
     * @inheritdoc
     */
    protected function configure() {
      $this
        ->setName('filldb')
        ->setDescription('This command creates a new table and populates it with values');
    }

      /**
       * @inheritdoc
       */
    protected function execute(InputInterface $input, OutputInterface $output) {
      $markers = json_decode(file_get_contents("KiewParsed.json"), true);
      $em = $this->getContainer()->get('doctrine.orm.entity_manager');
      $user = $em->getRepository('SiteBundle:Users\User')->findOneByEmail('eugene17green@gmail.com');
      foreach ($markers as $markerJson) {
        $markerEntity = new MarkerModel();
        $markerEntity->setPositionLat($markerJson['latitude']);
        $markerEntity->setPositionLng($markerJson['longitude']);
        $markerEntity->setTitle($markerJson['title']);
        $markerEntity->setAddress($markerJson['address']);
        $markerEntity->setDescription($markerJson['description']);
        $markerEntity->addImgUrl($markerJson['imageUrl']);
        $markerEntity->setStatus(MarkerModel::STATUS_MODERATION);
        $markerEntity->setUser($user);
        $em->persist($markerEntity);
      }
      $em->flush();
    }
  }