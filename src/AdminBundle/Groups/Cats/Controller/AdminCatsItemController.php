<?php

  namespace AdminBundle\Groups\Cats\Controller;

  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use SiteBundle\Entity\Cats\CatsModel;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Symfony\Component\HttpFoundation\Request;

  /**
   *
   * @package AdminBundle\Groups\Markers\Controller
   */
  class AdminCatsItemController extends Controller {

    /**
     * @Route("/cats/item/", name="adm-cats-item")
     */
    public function indexAction(Request $request) {
      $categoryId = $request->query->getInt('id');
      if (!empty($categoryId)) {
        $category = $this->getDoctrine()->getRepository(CatsModel::class)->find($categoryId);
      }
      if (empty($category)) {
        return $this->redirect($this->generateUrl('adm-cats-list'));
      }

      if ($request->isMethod(Request::METHOD_POST)) {
        $category->setName($request->request->get('name'));
        $this->getDoctrine()->getManager()->flush();
      }

      if ($request->query->getInt('delete') == 1) {
        $this->getDoctrine()->getManager()->remove($category);
        $this->getDoctrine()->getManager()->flush();
        $this->redirect($this->generateUrl('adm-cats-list'));
      }

      return $this->render('AdminBundle:cats:item.html.twig', [
        'category' => $category,
      ]);
    }
  }