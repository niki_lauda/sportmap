<?php

  namespace AdminBundle\Groups\Cats\Controller;

  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use SiteBundle\Entity\Cats\CatsModel;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Symfony\Component\HttpFoundation\Request;

  /**
   *
   * @package AdminBundle\Groups\Markers\Controller
   */
  class AdminCatsListController extends Controller {

    /**
     * @Route("/cats/list/", name="adm-cats-list")
     */
    public function indexAction(Request $request) {

      if ($request->isMethod(Request::METHOD_POST)) {
        $newCategoryName = $request->request->get('newCategory');
        if (!empty($newCategoryName)) {
          $newCategory = new CatsModel();
          $newCategory->setName($newCategoryName);
          $this->getDoctrine()->getManager()->persist($newCategory);
          $this->getDoctrine()->getManager()->flush();
        }
      }

      $catsList = $this->getDoctrine()->getRepository(CatsModel::class)->findAll();

      return $this->render('AdminBundle:cats:list.html.twig', [
        'catsList' => $catsList,
      ]);
    }
  }