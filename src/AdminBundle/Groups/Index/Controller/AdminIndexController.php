<?php

  namespace AdminBundle\Groups\Index\Controller;

  use Lib\Application\Admin\AdminBaseController;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use SiteBundle\Entity\MarkerModel;
  use Symfony\Component\HttpFoundation\Request;

  /**
   *
   * @package AdminBundle\Groups\Markers\Controller
   */
  class AdminIndexController extends AdminBaseController {
    
    /**
     * @Route("/", name="adm-home")
     */
    public function indexAction(Request $request) {
      return $this->render('AdminBundle:index:index.html.twig');
    }
  }