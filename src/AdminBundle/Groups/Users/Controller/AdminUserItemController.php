<?php

  namespace AdminBundle\Groups\Users\Controller;

  use Doctrine\ORM\Tools\Pagination\Paginator;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use SiteBundle\Entity\Users\User;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Symfony\Component\HttpFoundation\Request;

  /**
   *
   * @package AdminBundle\Groups\Markers\Controller
   */
  class AdminUserItemController extends Controller {

    /**
     * @Route("/users/item/", name="adm-user-item")
     */
    public function indexAction(Request $request) {
      $userId = $request->query->getInt('id');
      if (!empty($userId)) {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);
      }
      if (empty($user)) {
        return $this->redirect($this->generateUrl('adm-user-list'));
      }

      if ($request->query->get('delete') == 1) {
        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($this->generateUrl('adm-user-list'));
      }

      $errorList = [];
      if ($request->isMethod(Request::METHOD_POST)) {
        $data = $request->request;

        $userName = $data->get('username');
        $userName = preg_replace('!\s!', '', $userName);
        $user->setUsername($userName);
        $user->setEmail($data->get('email'));
        $user->setPassword($data->get('password'));

        $firstExistEmail = $this->getDoctrine()->getRepository(User::class)->createQueryBuilder('u')
          ->where('u.email = :email')->setParameter('email', $user->getEmail())
          ->where('u.id != :id')->setParameter('id', $user->getId())
          ->getQuery()->getFirstResult();
        if (!empty($firstExistEmail)) {
          $errorList[] = 'Користувач з таким email вже існує.';
        }

        $firstExistUserName = $this->getDoctrine()->getRepository(User::class)->createQueryBuilder('u')
          ->where("u.username = :username and u.id != :id")->setParameters([':username' => $user->getUsername(), ':id' => $user->getId()])
          ->getQuery()->getResult();
        if (!empty($firstExistUserName)) {
          $errorList[] = 'Користувач з таким логіном вже існує.';
        }

        if (empty($errorList)) {
          $this->getDoctrine()->getManager()->flush();
        }
      }

      return $this->render('AdminBundle:users:item.html.twig', [
        'user' => $user,
        'errorList' => $errorList,
      ]);
    }
  }