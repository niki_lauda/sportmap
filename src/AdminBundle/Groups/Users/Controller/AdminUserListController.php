<?php

  namespace AdminBundle\Groups\Users\Controller;

  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use SiteBundle\Entity\Users\User;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Symfony\Component\HttpFoundation\Request;

  /**
   *
   * @package AdminBundle\Groups\Markers\Controller
   */
  class AdminUserListController extends Controller {
    
    /**
     * @Route("/users/list/", name="adm-user-list")
     */
    public function indexAction(Request $request) {

      $query = $this->getDoctrine()->getRepository(User::class)->createQueryBuilder('u');
      $query->orderBy('u.id', 'desc');
      $itemsNum = (new \Doctrine\ORM\Tools\Pagination\Paginator($query))->count();

      $paginator = \Lib\Application\Db\Paginator::createFromRequest($request, $itemsNum);
      $query = $paginator->prepareQuery($query);

      $userList = $query->getQuery()->getResult();

      return $this->render('AdminBundle:users:list.html.twig', [
        'userList' => $userList,
        'paginator' => $paginator,
      ]);
    }
  }