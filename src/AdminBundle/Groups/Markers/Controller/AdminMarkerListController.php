<?php

  namespace AdminBundle\Groups\Markers\Controller;

  use Doctrine\ORM\Tools\Pagination\Paginator;
  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use SiteBundle\Entity\MarkerModel;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Symfony\Component\HttpFoundation\JsonResponse;
  use Symfony\Component\HttpFoundation\Request;

  /**
   *
   * @package AdminBundle\Groups\Markers\Controller
   */
  class AdminMarkerListController extends Controller {

    /**
     * @Route("/markers/search/", name="adm-marker-list")
     */
    public function indexAction(Request $request) {

      $currentPage = $request->query->getInt('page', 1);
      $perPage = 15;

      $query = $this->getDoctrine()->getRepository(MarkerModel::class)->createQueryBuilder('m');
      $query->setFirstResult($currentPage * ($perPage - 1));
      $query->setMaxResults($perPage);
      $query->orderBy('m.id', 'desc');

      $totalItemsNum = (new Paginator($query))->count();

      $nextPage = null;
      if ($totalItemsNum > $currentPage * $perPage) {
        $nextPage = $currentPage + 1;
      }
      $prevPage = $currentPage > 1 ? $currentPage - 1 : null;

      /** @var MarkerModel[] $markerList */
      $markerList = $query->getQuery()->getResult();

      if ($request->isXmlHttpRequest() and $request->query->has('delete')) {
        $deleteId = $request->query->getInt('delete');
        if (!empty($deleteId)) {
          $marker = $this->getDoctrine()->getRepository(MarkerModel::class)->find($deleteId);
        }
        if (empty($marker)) {
          return new JsonResponse(['result' => 'fail']);
        }
        $this->getDoctrine()->getManager()->remove($marker);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(['result' => 'ok']);
      }

      return $this->render('AdminBundle:markers:list.html.twig', [
        'markerList' => $markerList,
        'nextPage' => $nextPage,
        'prevPage' => $prevPage,
      ]);
    }
  }