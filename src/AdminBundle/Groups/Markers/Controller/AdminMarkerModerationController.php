<?php

  namespace AdminBundle\Groups\Markers\Controller;

  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Symfony\Component\HttpFoundation\Request;

  /**
   *
   * @package AdminBundle\Groups\Markers\Controller
   */
  class AdminMarkerModerationController extends Controller {
    
    /**
     * @Route("/markers/moderation/", name="adm-marker-moderation")
     */
    public function indexAction(Request $request) {
      return $this->render('AdminBundle:index:index.html.twig');
    }
  }