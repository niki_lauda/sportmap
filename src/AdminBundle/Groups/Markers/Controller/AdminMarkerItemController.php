<?php

  namespace AdminBundle\Groups\Markers\Controller;

  use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
  use SiteBundle\Entity\Cats\CatsModel;
  use SiteBundle\Entity\MarkerModel;
  use Symfony\Bundle\FrameworkBundle\Controller\Controller;
  use Symfony\Component\HttpFoundation\JsonResponse;
  use Symfony\Component\HttpFoundation\Request;

  /**
   *
   * @package AdminBundle\Groups\Markers\Controller
   */
  class AdminMarkerItemController extends Controller {

    /**
     * @Route("/markers/item/", name="adm-marker-item")
     */
    public function indexAction(Request $request) {
      $markerId = $request->query->getInt('id');
      if (!empty($markerId)) {
        $marker = $this->getDoctrine()->getRepository(MarkerModel::class)->find($markerId);
      }
      if (empty($marker)) {
        return $this->redirect($this->generateUrl('adm-marker-list'));
      }

      if ($request->isXmlHttpRequest() and $request->query->has('delete')) {
        $this->getDoctrine()->getManager()->remove($marker);
        return new JsonResponse(['result' => 'ok', 'redirect' => $this->generateUrl('adm-marker-list')]);
      }

      if ($request->isXmlHttpRequest() and $request->query->has('newStatus')) {
        $marker->setStatus($request->query->get('newStatus'));
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(['result' => 'ok']);
      }

      if ($request->isXmlHttpRequest() and $request->request->has('newImageUrl')) {
        $imageUrl = $request->request->get('newImageUrl');
        if (!empty($imageUrl)) {
          $marker->addImgUrl($imageUrl);
          $this->getDoctrine()->getManager()->flush();
        }
        return new JsonResponse(['result' => 'ok']);
      }

      if ($request->isXmlHttpRequest() and $request->request->has('removeImageUrl')) {
        $imageUrl = $request->request->get('removeImageUrl');
        if (!empty($imageUrl)) {
          $marker->removeImageUrl($imageUrl);
          $this->getDoctrine()->getManager()->flush();
        }
        return new JsonResponse(['result' => 'ok']);
      }

      $errorList = [];
      if ($request->isMethod(Request::METHOD_POST)) {
        $postBag = $request->request;
        $marker->setTitle($postBag->get('title'));
        $marker->setDescription($postBag->get('description'));
        $marker->setPositionLat($postBag->get('position_lat'));
        $marker->setPositionLng($postBag->get('position_lng'));

        $categoriesQuery = $this->getDoctrine()->getRepository(CatsModel::class)->createQueryBuilder('c');
        $selectedCategoryIds = (array) $postBag->get('categories');
        if (!empty($selectedCategoryIds)) {
          $categoriesQuery->add('where', $categoriesQuery->expr()->in('c.id', $selectedCategoryIds));

          $selectedCategories = $categoriesQuery->getQuery()->getResult();
          $marker->setCategories($selectedCategories);
          $this->getDoctrine()->getManager()->flush();
        }

        $errorList = $this->getMarkerErrors($marker);
        if (empty($errorList)) {
          $this->getDoctrine()->getManager()->flush();
        }
      }

      /** @var CatsModel[] $categoryList */
      $categoryList = $this->getDoctrine()->getRepository(CatsModel::class)->findAll();

      return $this->render('AdminBundle:markers:item.html.twig', [
        'marker' => $marker,
        'errorList' => $errorList,
        'categoryList' => $categoryList,
      ]);
    }


    /**
     * @param MarkerModel $marker
     * @return array
     */
    private function getMarkerErrors(MarkerModel $marker) {
      $errorList = [];
      if (empty($marker->getTitle())) {
        $errorList[] = 'Не заповнено поле "Заголовок"';
      }
      if (empty($marker->getDescription())) {
        $errorList[] = 'Не заповнено поле "Опис"';
      }
      return $errorList;
    }
  }