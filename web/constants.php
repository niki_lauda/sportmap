<?php
  if (preg_match('!^(?<dir>[a-z0-9]+).(debug|admin).sportmap.xyz$!', $_SERVER['HTTP_HOST'], $match)) {
    \Lib\Application\Environment::setIsDebug(true);
    \Lib\Application\Environment::setDevBrunch($match['dir']);
    define('STATIC_URL', 'http://' . \Lib\Application\Environment::getDevBrunch() . '.debug.static.sportmap.xyz/');
    define('IMG_URL', 'http://' . \Lib\Application\Environment::getDevBrunch() . '.debug.img.sportmap.xyz/');
  }  else{
    define('STATIC_URL', 'http://static.sportmap.xyz/');
    define('IMG_URL', 'http://img.sportmap.xyz/');
  }

  /**
   * Url to libs files
   */
  define('LIBS_URL', STATIC_URL . 'libs/');

  /**
   * Url to css files
   */
  define('CSS_URL', STATIC_URL . 'css/');

  /**
   * Url to js files
   */
  define('JS_URL', STATIC_URL . 'js/');

  
  
