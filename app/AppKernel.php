<?php

  use Lib\Application\Environment;
  use Symfony\Component\HttpFoundation\Request;
  use Symfony\Component\HttpKernel\HttpKernelInterface;
  use Symfony\Component\HttpKernel\Kernel;
  use Symfony\Component\Config\Loader\LoaderInterface;
  use Symfony\Component\Routing\Generator\UrlGenerator;

  /**
   * Class AppKernel
   */
  class AppKernel extends Kernel {

    /**
     *
     */
    public function boot() {
      parent::boot();

      $context = $this->getContainer()->get('router')->getContext();
      if ($devBranch = Environment::getDevBrunch()) {
        $context->setParameter('_hostAdmin', $devBranch . '.admin.sportmap.xyz');
        $context->setParameter('_hostSite', $devBranch . '.debug.sportmap.xyz');
      }
    }


    /**
     * @return array
     */
    public function registerBundles() {
      $bundles = [
        new WhiteOctober\BreadcrumbsBundle\WhiteOctoberBreadcrumbsBundle(),
        new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
        new Symfony\Bundle\SecurityBundle\SecurityBundle(),
        new Symfony\Bundle\TwigBundle\TwigBundle(),
        new Symfony\Bundle\MonologBundle\MonologBundle(),
        new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
        new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
        new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
        new SiteBundle\SiteBundle(),
        new AdminBundle\AdminBundle(),
      ];

      if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
        $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
        $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
        $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
        $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
      }

      return $bundles;
    }


    /**
     * @return string
     */
    public function getRootDir() {
      return __DIR__;
    }


    /**
     * @return string
     */
    public function getCacheDir() {
      return dirname(__DIR__) . '/var/cache/' . $this->getEnvironment();
    }


    /**
     * @return string
     */
    public function getLogDir() {
      return dirname(__DIR__) . '/var/logs';
    }


    /**
     * @param LoaderInterface $loader
     */
    public function registerContainerConfiguration(LoaderInterface $loader) {
      $loader->load($this->getRootDir() . '/config/config_' . $this->getEnvironment() . '.yml');
    }


    /**
     * {@inheritdoc}
     */
    public function handle(Request $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true) {
      $result = parent::handle($request, $type, $catch);

      $context = $this->getContainer()->get('router')->getContext();
      $context->setParameter('_host', $request->attributes->get('_host'));
      $context->setParameter('_dev', $request->attributes->get('_dev'));
      //echo "\n***".__LINE__."***\n<pre>".print_r($context, true)."</pre>\n";die();

      return $result;
    }


  }
